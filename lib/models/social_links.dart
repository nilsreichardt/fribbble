import 'package:meta/meta.dart';


class SocialLinks {
  final String github;
  final String twitter;
  final String instagram;
  final String linkedin;

  SocialLinks._({
    @required this.github,
    @required this.twitter,
    @required this.instagram,
    @required this.linkedin,
  });

  factory SocialLinks.create() {
    return SocialLinks._(
        github: "",
        twitter: "",
        instagram: "",
        linkedin: "", 
    );
  }

factory SocialLinks.fromData(dynamic data) {
    return SocialLinks._(
      github: data['github'], 
      twitter: data['twitter'],
      instagram: data['instagram'], 
      linkedin: data['linkedin'],
      );
  }

  Map<String, dynamic> toJson() {
    return {
      'github': github,
      'twitter': twitter,
      'instagram': instagram,
      'linkedin': linkedin,
    };
  }

  SocialLinks copyWith({
    String github, String twitter, String instagram, String linkedin,
  }){
    return SocialLinks._(
        github: github ?? this.github,
        twitter: twitter ?? this.twitter,
        instagram: instagram ?? this.instagram,
        linkedin: linkedin ?? this.linkedin,
    );
  }

}
