import 'package:flutter/material.dart';

void snackbarSoon({BuildContext context, GlobalKey<ScaffoldState> key}) {
  showSnackSec(
      text: "Diese Funktion ist bald verfügbar! 😊",
      seconds: 3,
      context: context,
      key: key);
}

void showSnack(
    {String text,
    Duration duration,
    BuildContext context,
    GlobalKey<ScaffoldState> key,
    bool withLoadingCircle = false}) {
  SnackBar snackBar = SnackBar(
    content: withLoadingCircle == false
        ? Text(text)
        : Container(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                const Padding(
                  padding: EdgeInsets.only(right: 20, left: 4),
                  child: SizedBox(
                    width: 25,
                    height: 25,
                    child: CircularProgressIndicator(),
                  ),
                ),
                Flexible(child: Text(text)),
              ],
            ),
          ),
    duration: duration,
  );

  if (key != null) {
    key.currentState.hideCurrentSnackBar();
    key.currentState.showSnackBar(snackBar);
  } else if (context != null) {
    Scaffold.of(context).hideCurrentSnackBar();
    Scaffold.of(context).showSnackBar(snackBar);
  } else {
    debugPrint("Fehler! Die SnackBar hat keinen Key und keinen Context!");
  }
}

void showSnackSec(
    {String text,
    int seconds,
    BuildContext context,
    GlobalKey<ScaffoldState> key,
    bool withLoadingCircle = false}) {
  showSnack(
      key: key,
      duration: Duration(seconds: seconds ?? 3),
      context: context,
      text: text,
      withLoadingCircle: withLoadingCircle);
}
