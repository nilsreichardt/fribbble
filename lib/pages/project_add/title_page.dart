import 'package:flutter/material.dart';
import 'package:fribbble/bloc/project_add_bloc.dart';
import 'package:fribbble/bloc_provider.dart';
import 'package:fribbble/pages/project_add/description_page.dart';
import 'package:fribbble/pages/project_add/next_button.dart';

class ProjectAddTitlePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final ProjectAddBloc bloc = ProjectAddBloc();
    return BlocProvider(
      bloc: bloc,
      child: _ProjectAddTitlePage(),
    );
  }
}

class _ProjectAddTitlePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<ProjectAddBloc>(context);
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Center(child: _TitleField()),
            NextPageButton(
              nextWidget: ProjectAddDescriptionPage(bloc: bloc),
            )
          ],
        ),
      ),
    );
  }
}

class _TitleField extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<ProjectAddBloc>(context);
    return TextField(
      decoration: InputDecoration(labelText: 'Title'),
      onChanged: bloc.changeTitle,
    );
  }
}
