import 'package:flutter/material.dart';
import 'package:fribbble/bloc/project_add_bloc.dart';
import 'package:fribbble/bloc_provider.dart';
import 'package:fribbble/util/snackbars.dart';

class NextPageButton extends StatelessWidget {
  const NextPageButton({Key key, this.nextWidget}) : super(key: key);

  final Widget nextWidget;

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      child: Text("Hi"),
      onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) => nextWidget)),
    );
  }
}

class FinishButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<ProjectAddBloc>(context);
    return RaisedButton(
      child: Text('Submit'),
      onPressed: () async {
        showSnackSec(
          text: "Sending data...",
          seconds: 60,
          withLoadingCircle: true,
          context: context,
        );

        try {
          await bloc.submit();
          Navigator.pop(context, true);
          Navigator.pop(context, true);
        } on Exception catch (e) {
          showSnackSec(
            text: e.toString(),
            seconds: 5,
            context: context,
          );
        }
      },
    );
  }
}
