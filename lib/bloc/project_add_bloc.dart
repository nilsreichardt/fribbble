import 'dart:io';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fribbble/gateway/feed_gateway.dart';
import 'package:fribbble/models/post.dart';

import 'project_add_validators.dart';
import 'package:fribbble/bloc_provider.dart';
import 'package:rxdart/rxdart.dart';

class ProjectAddBloc extends BlocBase {
  final _titleSubject = BehaviorSubject<String>();
  final _descriptionSubject = BehaviorSubject<String>();
  final _pictureFilesSubject = BehaviorSubject<List<File>>();
  final _appStoreActive = BehaviorSubject<bool>(seedValue: false);
  final _playStoreActive = BehaviorSubject<bool>(seedValue: false);
  final _appStoreLink = BehaviorSubject<String>();
  final _playStoreLink = BehaviorSubject<String>();

  Stream<String> get title => _titleSubject;
  Stream<String> get description => _descriptionSubject;
  Stream<List<File>> get pictureFiles => _pictureFilesSubject;
  Stream<bool> get appStoreActive => _appStoreActive;
  Stream<String> get appStoreLink => _appStoreLink;
  Stream<bool> get playStoreActive => _playStoreActive;
  Stream<String> get playStoreLink => _titleSubject;

  Function(String) get changeTitle => _titleSubject.sink.add;
  Function(String) get changeDescription => _descriptionSubject.sink.add;
  Function(bool) get changeAppStoreActive => _appStoreActive.sink.add;
  Function(bool) get changePlayStoreActive => _playStoreActive.sink.add;
  Function(String) get changeAppStoreLink => _appStoreLink.sink.add;
  Function(String) get changePlayStoreLink => _playStoreLink.sink.add;

  Function(File) get addFile => (file) {
        final list = <File>[];
        if (_pictureFilesSubject.value != null) {
          list.addAll(_pictureFilesSubject.value);
        }
        list.add(file);
        _pictureFilesSubject.sink.add(list);
      };
  Function(File) get removeFile => (file) {
        final list = <File>[];
        if (_pictureFilesSubject.value != null) {
          list.addAll(_pictureFilesSubject.value);
        }
        list.remove(file);
        _pictureFilesSubject.sink.add(list);
      };

  Future<void> submit() async {
    final String title = _titleSubject.value;
    final String description = _descriptionSubject.value;

    final DatabasePost post = DatabasePost.create().copyWith(
      title: title,
      description: description,
    );

    await Firestore.instance.collection('posts').document().setData(post.toJson());
  }

  @override
  void dispose() {
    _titleSubject.close();
  }
}
